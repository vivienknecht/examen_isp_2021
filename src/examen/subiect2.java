package examen;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Random;

public class subiect2 extends JFrame {

    HashMap examen = new HashMap();

    JButton press;
    JTextArea text;

    subiect2() {
        setTitle("Random");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 500);
        init();
        setVisible(true);

    }

    public void init() {
        this.setLayout(null);
        press = new JButton("Click me");
        press.setBounds(100, 200, 200, 30);
        text = new JTextArea();
        text.setEditable(false);
        text.setBounds(100, 100, 200, 50);
        add(press);
        add(text);
        press.addActionListener(new randNumber());
    }



    public static void main(String[] args) {
        new subiect2();
    }

    class randNumber implements ActionListener{


        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Random rand = new Random();
                int nr = rand.nextInt();
                String s = String.valueOf(nr);
                        text.setText(s);
                text.append(s);
            } catch (Exception exception) {
            }
        }
        }

        }


