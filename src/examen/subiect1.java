package examen;

public class subiect1 {
    class U {
        public void p(){}
    }

    class I extends U{
        private long t;
        public K myK;

        public void f(){}
    }

    class J {
        public void i(I I){}
    }

    class N {
        I myI;
    }

    class K{
        L[] list;
        M myM;

        K(L[] list, M mm)
        {
            this.list = list;
            this.myM = mm;
        }

    }
    class L{

        public void metA(){}

    }

    class M {
        public void metB(){}

    }

}
